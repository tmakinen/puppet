# Install sudo and sudoers file.
#
class sudo {

    if $::operatingsystem != "OpenBSD" {
        package { "sudo":
            ensure => installed,
            before => File["/etc/sudoers.d"],
        }
    }

    file { "/etc/sudoers.d":
        ensure  => directory,
        mode    => "0440",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        purge   => true,
        force   => true,
        recurse => true,
        source  => "puppet:///modules/custom/empty",
    }

    file { "/etc/sudoers":
        ensure  => present,
        mode    => "0440",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source  => "puppet:///modules/sudo/sudoers",
        require => File["/etc/sudoers.d"],
    }

}


# Install fake sudo
#
class sudo::fake {

  file { "/usr/bin/sudo":
    ensure => present,
    mode   => "0555",
    owner  => "root",
    group  => "root",
    source => "puppet:///modules/sudo/fakesudo",
  }

}


# Add sudoer.
#
# === Parameters
#
#   $who:
#       User or group. Prefix group name with %. Defaults to $name.
#   $where:
#       Defaults to ALL.
#   $as_whom:
#       Defaults to ALL.
#   $what:
#       Defaults to ALL.
#
define sudo::sudoer(
    $who=undef,
    $where="ALL",
    $as_whom="ALL",
    $what="ALL"
) {

    if ! $who {
        $who_real = $name
    } else {
        $who_real = $who
    }

    $name_real = regsubst($name, '%', '_')

    file { "/etc/sudoers.d/${name_real}":
        ensure  => present,
        mode    => "0440",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        content => "${who_real}\t${where}=(${as_whom})\t${what}\n",
        require => File["/etc/sudoers"],
    }

}


# Disable sudo
#
# Cannot remove sudo package itself due to depencies
#
class sudo::disable {

    exec { "chmod 0000 /usr/bin/sudo":
        user   => "root",
        path   => "/bin:/usr/bin:/sbin:/usr/sbin",
        onlyif => "test -u /usr/bin/sudo",
    }

}

