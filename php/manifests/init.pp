
# Install PHP command-line interface
#
class php::cli {

    package { "php-cli":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => "php5-cli",
            "ubuntu" => "php5-cli",
            default  => "php-cli",
        },
    }

}


# Install GD support to PHP
#
class php::gd {

    package { "php-gd":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => "php5-gd",
            "ubuntu" => "php5-gd",
            default  => "php-gd",
        },
    }

}


# Install MySQL support to PHP
#
class php::mysql {

    package { "php-mysql":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => "php5-mysql",
            "ubuntu" => "php5-mysql",
            default  => "php-mysql",
        },
    }

}


# Install PostgreSQL support to PHP
#
class php::pgsql {

    package { "php-pgsql":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => "php5-pgsql",
            "ubuntu" => "php5-pgsql",
            default  => "php-pgsql",
        },
    }

}


# Install PDO database abstraction support to PHP
#
class php::pdo {

    if $::operatingsystem in ["CentOS","RedHat"] {
        package { "php-pdo":
            ensure => installed,
        }
    }

}


# Install DBA suppor to PHP
#
class php::dba {

    if $::operatingsystem in ["CentOS","RedHat"] {
        package { "php-dba":
            ensure => installed,
        }
    }

}


# Install IMAP support to PHP
#
class php::imap {

    package { "php-imap":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => "php5-imap",
            "ubuntu" => "php5-imap",
            default  => "php-imap",
        },
    }

}


# Install LDAP support to PHP
#
class php::ldap {

    package { "php-ldap":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => "php5-ldap",
            "ubuntu" => "php5-ldap",
            default  => "php-ldap",
        },
    }

}


# Install Multibyte String support to PHP
#
class php::mbstring {

    if $::operatingsystem in ["CentOS","RedHat"] {
        package { "php-mbstring":
            ensure => installed,
        }
    }

}


# Install Mcrypt support to PHP
#
class php::mcrypt {

    package { "php-mcrypt":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => "php5-mcrypt",
            "ubuntu" => "php5-mcrypt",
            default  => "php-mcrypt",
        },
    }

}


# Install PEAR support to PHP
#
class php::pear {

    package { "php-pear":
        ensure => installed,
    }

}


# Install XML support to PHP
#
class php::xml {

    if $::operatingsystem in ["CentOS","RedHat"] {
        package { "php-xml":
            ensure => installed,
        }
    }

}
