#!/bin/sh

LOGDIR="/srv/log"
ARCHIVE="${LOGDIR}/archive"

DATE="`date +%Y-%m-%d`"
YEAR="`date +%Y`"

umask 027

myerror()
{
    echo "Error: $*" 1>&2
    exit 1
}

archive_log()
{
    FILE="${1}"
    DEST="${2}"

    if [ -f "${DEST}" -o -f "${DEST}.gz" ]; then
        echo "Skipping ${FILE}: Archive already exists" 1>&2
    else
        echo "Archiving file ${FILE} to ${DEST}"
        mv "${FILE}" "${DEST}"
        touch "${FILE}"
        LOGS="${LOGS} ${DEST}"
    fi
}

restart_syslog()
{
    for i in syslog.pid rsyslogd.pid syslogd.pid ; do
        if [ -f "/var/run/$i" ]; then
            PIDFILE="/var/run/$i"
            break
        fi
    done
    if [ "blah${PIDFILE}" = "blah" ]; then
        myerror "Cannot find syslog pid file"
    fi
    kill -HUP `cat ${PIDFILE}`
}

[ $# -gt 0 ] || myerror "Usage: `basename $0` <file|dir> [file|dir] ..."

[ -d ${LOGDIR} ] || myerror "Not a directory: ${LOGDIR}"

while [ "$*" ]; do
    if [ -f "${LOGDIR}/${1}" ]; then
        dstdir=${ARCHIVE}/${YEAR}
        dstfile=${dstdir}/`basename ${1}`.${DATE}
        [ -d "${dstdir}" ] || mkdir -p ${dstdir}
        archive_log ${LOGDIR}/${1} ${dstfile}
    elif [ -d "${LOGDIR}/${1}" ]; then
        for f in ${LOGDIR}/${1}/*.log; do
            if [ -f "${f}" ]; then
                dstdir=${ARCHIVE}/${1}/${YEAR}
                dstfile=${dstdir}/`basename ${f}`.${DATE}
                [ -d "${dstdir}" ] || mkdir -p ${dstdir}
                archive_log ${f} ${dstfile}
            else
                echo "Skipping ${f}: not a file" 1>&2
            fi
        done
    else
        echo "Skipping ${1}: not a file or directory" 1>&2
    fi
    shift
done

restart_syslog

for log in ${LOGS}; do
    gzip -f ${log} || myerror "Error while gzipping ${log}"
    loggz="`basename ${log}`.gz"
    ( cd `dirname ${log}` && openssl sha1 -out ${loggz}.sha1 ${loggz} )
done
