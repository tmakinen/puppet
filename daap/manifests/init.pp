
# Install daap server.
#
class daap::server {

    include avahi::daemon

    package { "mt-daapd":
        ensure => installed,
    }

    service { "mt-daapd":
        ensure  => running,
        enable  => true,
        require => [ Package["mt-daapd"], Service["avahi-daemon"], ],
    }

    file { "/etc/mt-daapd.conf":
        ensure  => present,
        source  => "puppet:///files/daap/mt-daapd.conf",
        mode    => "0640",
        owner   => root,
        group   => mt-daapd,
        require => Package["mt-daapd"],
        notify  => Service["mt-daapd"],
    }

}
