#!/bin/sh

echo "# Generated by $0" > /etc/mail/gecos

/usr/bin/awk -F : '{
    if($3 != "0" && $7 != "/sbin/nologin") {
        gsub(/[[:space:]]+/, ".", $5);
        print tolower($5)":"$1;
    }
}' /etc/passwd >> /etc/mail/gecos
