# Install etherpad-lite.
#
class etherpadlite {

    include nodejs

    include user::system
    realize(User["etherpad"], Group["etherpad"])

    file { "/var/lib/etherpad":
        ensure  => "directory",
        mode    => "0700",
        owner   => "etherpad",
        group   => "etherpad",
        require => User["etherpad"],
    }
    file { "/var/lib/etherpad/.profile":
        ensure  => present,
        mode    => "0600",
        owner   => "etherpad",
        group   => "etherpad",
        content => "umask 007\n",
        require => File["/var/lib/etherpad"],
    }

}
