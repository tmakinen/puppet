# Install common parts of DHCP server
#
class dhcp::server::common($interface=undef) {

    case $::operatingsystem {
        "centos","redhat": {
            if versioncmp($::operatingsystemrelease, "6") >= 0 {
                $confdir = "/etc/dhcp"
            } else {
                $confdir = "/etc"
            }
            $package = "dhcp"
            $service = "dhcpd"
        }
        "fedora": {
            $confdir = "/etc/dhcp"
            $package = "dhcp"
            $service = "dhcpd"
        }
        "openbsd": {
            $confdir = "/etc"
            $package = "isc-dhcp-server"
            $service = "isc_dhcpd"

            Service["dhcpd"] {
                flags => $interface ? {
                    undef   => "-q -user _isc-dhcp -group _isc-dhcp",
                    default => "-q -user _isc-dhcp -group _isc-dhcp ${interface}",
                },
            }
        }
        "ubuntu": {
            if versioncmp($::operatingsystemrelease, "12.04") >= 0 {
                $confdir = "/etc/dhcp"
                $package = "isc-dhcp-server"
                $service = "isc-dhcp-server"
            } else {
                $confdir = "/etc/dhcp3"
                $service = "dhcp3-server"
                $package = "dhcp3-server"
            }
        }
        default: {
            fail("dhcp::server not supported on ${::operatingsystem}")
        }
    }

    package { "dhcp":
        ensure => installed,
        name   => $package,
    }

    if $::operatingsystem == "OpenBSD" {
        if versioncmp($::operatingsystemrelease, "5.8") < 0 {
            file { "/etc/rc.d/isc_dhcpd":
                ensure => present,
                mode   => "0555",
                owner  => "root",
                group  => "bin",
                source => "puppet:///modules/dhcp/isc_dhcpd.rc",
                before => Service["dhcpd"],
            }
            file { "/var/db/dhcpd.leases":
                ensure => present,
                mode   => "0644",
                owner  => "root",
                group  => "wheel",
                before => Service["dhcpd"],
            }
        }
    }

    service { "dhcpd":
        ensure  => running,
        enable  => true,
        name    => $service,
        require => Package["dhcp"],
    }

    if $interface {
        case $::operatingsystem {
            "fedora","centos","redhat": {
                file { "/etc/sysconfig/dhcpd":
                    ensure  => present,
                    mode    => "0644",
                    owner   => "root",
                    group   => "root",
                    content => template("dhcp/dhcpd.sysconfig"),
                    require => Package["dhcp"],
                    notify  => Service["dhcpd"],
                }
            }
            "openbsd": {}
            default: {
                fail("Parameter interface not supported in ${::operatinsystem}")
            }
        }
    }

}


# Install DHCP server with static config.
#
# === Parameters
#
#   $interface:
#       Interface that DHCP server should listen. Defaults to all.
#
class dhcp::server($interface=undef) {

    class { "dhcp::server::common":
        interface => $interface,
    }

    file { "dhcpd.conf":
        ensure => present,
        name   => "${dhcp::server::common::confdir}/dhcpd.conf",
        mode   => "0644",
        owner  => "root",
        group  => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source => [
            "puppet:///files/dhcp/dhcpd.conf.${::homename}",
            "puppet:///files/dhcp/dhcpd.conf",
        ],
        notify => Service["dhcpd"],
    }

}


# Install DHCP server and generate config from LDAP.
#
# === Parameters
#
#   $interface:
#       Interface that DHCP server should listen. Defaults to all.
#
class dhcp::server::ldap($interface=undef) {

    class { "dhcp::server::common":
        interface => $interface,
    }

    require python
    require ldap::client

    file { "/usr/local/sbin/dhcpdump.py":
        ensure => present,
        mode   => "0755",
        owner  => "root",
        group  => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source => "puppet:///modules/dhcp/dhcpdump.py",
    }

    $confdir = $dhcp::server::common::confdir

    file { "dhcpd.conf.in":
        ensure  => present,
        name    => "${confdir}/dhcpd.conf.in",
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source  => [
            "puppet:///files/dhcp/dhcpd.conf.in.${::hostname}",
            "puppet:///files/dhcp/dhcpd.conf.in",
        ],
        require => Package["dhcp"],
    }

    exec { "generate-dhcp-conf":
        path    => "/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin",
        command => "dhcpdump.py ${confdir}/dhcpd.conf.in* > ${confdir}/dhcpd.conf",
        unless  => "dhcpdump.py ${confdir}/dhcpd.conf.in* | diff ${confdir}/dhcpd.conf -",
        require => File["dhcpd.conf.in", "/usr/local/sbin/dhcpdump.py"],
        notify  => Service["dhcpd"],
    }

}
