
# Install Icecast server
#
# === Parameters:
#
#     $sourcepass:
#         Password for stream sources.
#
#     $adminpass:
#         Password for admin pages.
#
class icecast::server($sourcepass, $adminpass) {

    package { "icecast":
        ensure => installed,
    }

    file { "/etc/icecast.xml":
        ensure  => present,
        content => template("icecast/icecast.xml.erb"),
        mode    => "0640",
        owner   => "root",
        group   => "icecast",
        require => Package["icecast"],
        notify  => Service["icecast"],
    }

    service { "icecast":
        ensure => running,
        enable => true,
    }

}
