Facter.add('console') do
    confine :kernel => :linux
    setcode do
        console = 'vga'
        File.readlines("/proc/cmdline").each do |line|
            if m = line.match(/console=(\S*)/)
                console = m[1]
            end
        end
        console
    end
end

Facter.add('console') do
    confine :kernel => :openbsd
    setcode do
        console = 'vga'
        if File.exists?("/etc/boot.conf")
            File.readlines("/etc/boot.conf").each do |line|
                if m = line.match(/set\s+tty\s+(\S+)/)
                    console = m[1]
                end
            end
        end
        console
    end
end
