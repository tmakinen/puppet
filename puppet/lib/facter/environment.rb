ENV.each do |k,v|
    next if not k.downcase =~ /^puppet_/
    Facter.add("env_#{k.downcase}".to_sym) do
        setcode do
            v
        end
    end
end
