# Install augeas tools.
#
class augeas {

  $package = $::operatingsystem ? {
    'ubuntu' => 'augeas-tools',
    default  => 'augeas',
  }

  package { 'augeas':
    ensure => installed,
    name   => $package,
  }

}
