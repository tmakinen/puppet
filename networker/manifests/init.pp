
# Install common parts of Legato Networker
#
class networker::common {

    require portmap::server

    $range = inline_template('7937-<%= 7936 + @ports.to_i %>')

    file { [ "/nsr", "/nsr/res", ]:
        ensure  => directory,
        mode    => "0755",
        owner   => "root",
        group   => "root",
    }

    package { "lgtoman":
        ensure => installed,
    }

    service { "networker":
        ensure    => running,
        enable    => true,
        hasstatus => true,
    }

    exec { "nsrports":
        command =>  "nsrports -S ${range}",
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        unless  => "nsrports | egrep '^Service ports: ${range}[[:space:]]$'",
        require => Service["networker"],
    }

}

# Install Legato Networker client
#
# === Global variables
#
#     $networker_servers:
#         Array containing networker servers allowed to backup this host.
#
class networker::client {

    if !$ports {
        $ports = 4
    }
    include networker::common

    package { "lgtoclnt":
        ensure => installed,
        before => File["/nsr"],
    }

    file { "/nsr/res/servers":
        ensure  => present,
        content => template("networker/servers.erb"),
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => File["/nsr/res"],
        notify  => Service["networker"],
    }

}


# Install Legato Networker server
#
# === Parameters:
#
#     $datadir:
#         Datadirectory for indexes.
#     $ports:
#         Number of ports to reserve for networker use. Defaults to 8.
#         See nsrports man page.
#
class networker::server($datadir="/nsr", $ports=8) inherits networker::common {

    $networker_server = [ $::homename, ]
    include networker::client

    package { [ "lgtolicm", "lgtonode", "lgtoserv", ]:
        ensure => installed,
        before => Service["networker"],
    }

    if $datadir != "/nsr" {
        file { $datadir:
            ensure => directory,
            mode   => "0755",
            owner  => "root",
            group  => "root",
        }
        File["/nsr"] {
            ensure  => link,
            target  => $datadir,
            require => File[$datadir],
        }
    }

}

