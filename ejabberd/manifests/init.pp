# Install ejabberd.
#
# === Parameters
#
# $collab:
#   Boolean for enabling collab integration. Defaults to false.
#
# $erlang_solutions:
#   Boolean for automatically enabling Erlang Solutions repository.
#   Defaults to true.
#
# $package:
#   Ejabberd package source. Required for collab integration.
#
# $hosts:
#   Array of domains serverd by ejabberd. Defaults to [ "$homename" ].
#
# $admins:
#   Array of users with admin privileges.
#
# $webhosts:
#   Array of BOSH virtual hosts.
#
# $auth:
#   Authentication method or array of multiple methods.
#   Valid values internal, external or ldap. Defaults to internal.
#
# $extauth:
#   Path to external authentication command.
#
# $extauth_cache:
#   Number of seconds that ejabberd can cache external authentication
#   information. Disabled by default.
#
# $max_user_sessions:
#   Maximum number of sessions per user. Defaults to 1000.
#
# $max_user_conferences:
#   Maximum number of conferences per user. Defaults to 2000.
#
# $muc_max_users:
#   Maximum number of users per conference. Defaults to 1000.
#
# $muclog_datadir:
#   Path where to store chatroom logs. Disabled by default.
#
# $muclog_default:
#   Enable room logging by default. Defaults to true.
#
# $muclog_format:
#   Chatroom log format. Valid values html or plaintext.
#
# $ssl_key:
#   Path to SSL private key.
#
# $ssl_cert:
#   Path to SSL certificate.
#
# $ssl_chain:
#   Path to SSL certificate chain.
#
# $ldap_server:
#   Array of LDAP authentication servers.
#
# $ldap_basedn:
#   LDAP base dn.
#
# $ldap_encrypt:
#   LDAP encryption. Defaults to "tls".
#
# $ldap_port:
#   LDAP port. Defaults to 636.
#
# $ldap_uid:
#   LDAP UID attribute. Defaults to "uid".
#
# $ldap_rootdn:
#   Optional bind DN.
#
# $ldap_password:
#   Bind DN password.
#
class ejabberd(
  $collab=false,
  $erlang_solutions=true,
  $package=undef,
  $hosts=[$::homename],
  $admins=undef,
  $webhosts=undef,
  $auth='internal',
  $extauth=undef,
  $extauth_cache=false,
  $max_user_sessions='1000',
  $max_user_conferences='2000',
  $muc_max_users='1000',
  $muclog_datadir=undef,
  $muclog_default=true,
  $muclog_format='plaintext',
  $ssl_key="${::puppet_ssldir}/private_keys/${::homename}.pem",
  $ssl_cert="${::puppet_ssldir}/certs/${::homename}.pem",
  $ssl_chain=undef,
  $ldap_server=undef,
  $ldap_basedn=undef,
  $ldap_encrypt='tls',
  $ldap_port='636',
  $ldap_uid='uid',
  $ldap_rootdn=undef,
  $ldap_password=undef
) {

  include user::system
  realize(User['ejabberd'], Group['ejabberd'])

  if ! ($muclog_format in [ 'html', 'plaintext' ]) {
    fail("Invalid value ${muclog_format} for muclog_format")
  }

  case $::operatingsystem {
    'centos','redhat','fedora': {
      $package_provider = 'rpm'

      if $package and versioncmp($package, 'ejabberd-13.10') >= 0 {
        $config = 'ejabberd.yml'
        $erlang_solutions_real = $erlang_solutions
        package { 'libyaml':
          ensure => installed,
          before => Package['ejabberd'],
        }
      } else {
        $config = 'ejabberd.cfg'
        $erlang_solutions_real = false
      }
    }
    'debian','ubuntu': {
      $package_provider = 'dpkg'

      if $package and versioncmp($package, 'ejabberd_13.10') >= 0 {
        $config = 'ejabberd.yml'
        $erlang_solutions_real = $erlang_solutions
      } else {
        $config = 'ejabberd.cfg'
        $erlang_solutions_real = false
      }
    }
    default: {
      fail("ejabberd not supported on ${::operatingsystem}.")
    }
  }

  class { 'erlang':
    erlang_solutions => $erlang_solutions_real,
    before           => Package['ejabberd'],
  }

  if $collab == true {
    if ! $package {
      fail('Must define package for collab integration')
    }

    file { "/usr/local/src/${package}":
      ensure => present,
      mode   => '0644',
      owner  => 'root',
      group  => 'root',
      source => "puppet:///files/packages/${package}",
      before => Package['ejabberd'],
    }

    Package['ejabberd'] {
      provider => $package_provider,
      source   => "/usr/local/src/${package}",
    }

    exec { 'usermod-ejabberd':
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => 'usermod -a -G collab ejabberd',
      unless  => "id -n -G ejabberd | grep '\\bcollab\\b'",
      require => [ User['ejabberd'], Group['collab'] ],
      notify  => Service['ejabberd'],
    }

    Service['ejabberd'] {
      require => Class['wiki::collab'],
    }

    if $muclog_datadir {
      file { $muclog_datadir:
        ensure  => directory,
        mode    => '2770',
        owner   => 'collab',
        group   => 'collab',
        require => User['collab'],
        before  => Service['ejabberd'],
      }
    }
  }

  package { 'ejabberd':
    ensure  => $collab ? {
      true    => latest,
      default => installed,
    },
    require => [ User['ejabberd'], Group['ejabberd'] ],
  }

  service { 'ejabberd':
    ensure => running,
    enable => true,
    status => 'ejabberdctl status >/dev/null',
  }

  include ssl

  file { "${ssl::private}/ejabberd.key":
    ensure => present,
    source => $ssl_key,
    mode   => '0600',
    owner  => 'root',
    group  => 'root',
    notify => Exec['generate-ejabberd-pem'],
  }
  file { "${ssl::certs}/ejabberd.crt":
    ensure => present,
    source => $ssl_cert,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    notify => Exec['generate-ejabberd-pem'],
  }
  if $ssl_chain {
    file { "${ssl::certs}/ejabberd.chain.crt":
      ensure => present,
      source => $ssl_chain,
      mode   => '0644',
      owner  => 'root',
      group  => 'root',
      notify => Exec['generate-ejabberd-pem'],
    }
    $cert_files = "${ssl::private}/ejabberd.key ${ssl::certs}/ejabberd.crt ${ssl::certs}/ejabberd.chain.crt"
  } else {
    $cert_files = "${ssl::private}/ejabberd.key ${ssl::certs}/ejabberd.crt"
  }

  exec { 'generate-ejabberd-pem':
    path        => '/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin',
    command     => "/bin/sh -c 'umask 077 ; cat ${cert_files} > /etc/ejabberd/ejabberd.pem'",
    refreshonly => true,
    before      => File['/etc/ejabberd/ejabberd.pem'],
    require     => Package['ejabberd'],
    notify      => Service['ejabberd'],
  }

  file { '/etc/ejabberd/ejabberd.pem':
    ensure  => present,
    mode    => '0640',
    owner   => 'root',
    group   => 'ejabberd',
    require => Package['ejabberd'],
  }

  file { "/etc/ejabberd/${config}":
    ensure  => present,
    mode    => '0640',
    owner   => 'root',
    group   => 'ejabberd',
    content => template("ejabberd/${config}.erb"),
    require => Package['ejabberd'],
    notify  => Service['ejabberd'],
  }

  case $::operatingsystem {
    'centos','redhat','fedora': {
      augeas { 'set-ejabberd-sysconfig':
        context => '/files/etc/sysconfig/ejabberd',
        changes => 'set ULIMIT_MAX_FILES 4096',
        require => Package['ejabberd'],
        notify  => Service['ejabberd'],
      }
    }
    'debian', 'ubuntu': {
      augeas { 'set-ejabberd-default':
        context => '/files/etc/default/ejabberd',
        changes => [ 'set POLL true', 'set SMP auto' ],
        require => Package['ejabberd'],
        notify  => Service['ejabberd'],
      }
    }
    default: { }
  }

  if $webhosts {
    include apache::mod::proxy
    include apache::mod::proxy_http
    include apache::mod::rewrite

    file { [
      '/usr/share/ejabberd',
      '/usr/share/ejabberd/htdocs',
    ]:
      ensure => directory,
      mode   => '0755',
      owner  => 'root',
      group  => 'root',
    }

    file { '/usr/share/ejabberd/htdocs/.htaccess':
      ensure  => present,
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      content => template('ejabberd/htaccess.erb'),
    }

    apache::configfile { 'ejabberd.conf':
      http   => false,
      source => 'puppet:///modules/ejabberd/ejabberd-httpd.conf',
    }

    selinux::manage_port { '5280':
      type  => 'http_port_t',
      proto => 'tcp',
    }

    ejabberd::configwebhost { $webhosts:
      htdocs => '/usr/share/ejabberd/htdocs',
    }
  }

}


# Enable bosh on virtual host.
#
define ejabberd::configwebhost($htdocs) {

  file { "/srv/www/https/${name}/bosh":
    ensure => link,
    target => $htdocs,
  }

}


# Install ejabberd backup cron script.
#
# === Parameters
#
# $datadir:
#   Path where to store the backups. Defaults to "/srv/ejabberd-backup".
#
class ejabberd::backup(
  $datadir='/srv/ejabberd-backup',
) {

  file { $datadir:
    ensure => directory,
    mode   => '0700',
    owner  => 'root',
    group  => 'root',
  }

  file { '/usr/local/sbin/ejabberd-backup':
    ensure  => present,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    content => template('ejabberd/ejabberd-backup.erb'),
  }

  cron { 'ejabberd-backup':
    ensure  => present,
    command => '/usr/local/sbin/ejabberd-backup',
    user    => 'root',
    minute  => '15',
    hour    => '21',
    require => File[$datadir, '/usr/local/sbin/ejabberd-backup'],
  }

}
