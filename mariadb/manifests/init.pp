
# Install MariaDB client utilities.
#
class mariadb::client {

    package { "mariadb":
        ensure => installed,
    }

}


# Install MariaDB server
#
# === Parameters
#
#   $datadir:
#       Directory where MariaDB databases are stored.
#
#   $root_password:
#       Password for MariaDB server root user.
#
class mariadb::server($datadir="/srv/mariadb", $config=undef, $root_password=undef) {

    package { "mariadb-server":
        ensure => installed,
    }

    if $datadir != "/srv/mariadb" {
        file { $datadir:
            ensure  => directory,
            mode    => "0755",
            owner   => "mysql",
            group   => "mysql",
            seltype => "mysqld_db_t",
            require => Package["mariadb-server"],
        }
        file { "/srv/mariadb":
            ensure  => link,
            target  => $datadir,
            seltype => "mysqld_db_t",
            require => File[$datadir],
        }
    } else {
        file { "/srv/mariadb":
            ensure  => directory,
            mode    => "0755",
            owner   => "mysql",
            group   => "mysql",
            seltype => "mysqld_db_t",
            require => Package["mariadb-server"],
        }
    }

    selinux::manage_fcontext { "/srv/mariadb(/.*)?":
        type   => "mysqld_db_t",
        before => File["/srv/mariadb"],
    }
    if $datadir {
        selinux::manage_fcontext { "${datadir}(/.*)?":
            type   => "mysqld_db_t",
            before => File[$datadir],
        }
    }

    service { "mariadb":
        ensure  => running,
        enable  => true,
        require => File["/srv/mariadb"],
    }

    file { "/etc/my.cnf":
        ensure  => present,
        source  => $config ? {
            undef   => "puppet:///modules/mariadb/my.cnf",
            default => $config,
        },
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["mariadb-server"],
        notify  => Service["mariadb"],
    }

    file { "/etc/logrotate.d/mariadb":
        ensure  => present,
        source  => "puppet:///modules/mariadb/mariadb.logrotate",
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["mariadb-server"],
    }

    case $root_password {
        undef: {
            file { "/var/lib/mysql":
                ensure  => directory,
                mode    => "0700",
                owner   => "mysql",
                group   => "mysql",
                require => Package["mariadb-server"],
            }
        }
        default: {
            file { "/root/.my.cnf":
                ensure  => present,
                content => "[client]\nuser=\"root\"\npassword=\"${root_password}\"\n",
                mode    => "0600",
                owner   => "root",
                group   => "root",
            }
        }
    }

}


# Install MariaDB daily backup job
#
# === Global variables
#
#   $datadir:
#       Directory where MariaDB backups are stored. Defaults
#       to /srv/mariadb-backup
#
#   $maxage:
#       How long to keep MariaDB backups. Defaults to 7 days.
#
class mariadb::server::backup($datadir="/srv/mariadb-backup", $maxage="7") {

    require mariadb::client

    file { $datadir:
        ensure  => directory,
        mode    => "0700",
        owner   => "root",
        group   => "root",
    }

    file { "/usr/local/sbin/mariadb-backup":
        ensure  => present,
        content => template("mariadb/mariadb-backup.cron.erb"),
        mode    => "0755",
        owner   => "root",
        group   => "root",
        require => File[$datadir],
    }

    cron { "mariadb-backup":
        command     => "/usr/local/sbin/mariadb-backup",
        user        => "root",
        hour        => "0",
        minute      => "30",
        require     => File["/usr/local/sbin/mariadb-backup"],
    }

}
