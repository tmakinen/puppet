# Common class for systemd.
#
class systemd {

  exec { 'systemctl daemon-reload':
    refreshonly => true,
    command     => $::operatingsystem ? {
      'ubuntu' => '/bin/systemctl daemon-reload',
      default  => '/usr/bin/systemctl daemon-reload',
    },
  }

}


# Install systemd unit file.
#
define systemd::unit(
  $content=undef,
  $source=undef,
) {

  include systemd

  file { "/etc/systemd/system/${name}":
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => $content,
    source  => $source,
    notify  => Exec['systemctl daemon-reload'],
  }

}
