
# Install S.M.A.R.T. tools
class smart {

    package { "smartmontools":
        ensure => installed
    }

}

# Enable S.M.A.R.T. monitoring daemon
#
class smart::daemon {

    require smart

    service { "smartd":
        ensure  => running,
        enable  => true,
        require => Package["smartmontools"],
    }

}
