# Install TeamSpeak server.
#
# === Parameters
#
# $license:
#   Teamspeak license file source.
#
class teamspeak($package=undef, $license=undef) {

  if ! $package {
    case $::architecture {
      'amd64','x86_64': {
        if $::teamspeak64_package_latest {
          $teamspeak_package = $::teamspeak64_package_latest
        } else {
          fail('Must define $teamspeak_package or $teamspeak64_package_latest')
        }
      }
      default: {
        if $::teamspeak32_package_latest {
          $teamspeak_package = $::teamspeak32_package_latest
        } else {
          fail('Must define $teamspeak_package or $teamspeak32_package_latest')
        }
      }
    }
  } else {
    $teamspeak_package = $package
  }

  file { '/usr/local/src/teamspeak3-server_linux.tar.bz2':
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    source => "puppet:///files/packages/${teamspeak_package}",
  }
  util::extract::tar { '/usr/local/teamspeak':
    ensure  => latest,
    strip   => 1,
    source  => '/usr/local/src/teamspeak3-server_linux.tar.bz2',
    require => File['/usr/local/src/teamspeak3-server_linux.tar.bz2'],
    notify  => Service['teamspeak'],
  }

  include user::system
  realize(User['teamspeak'], Group['teamspeak'])

  file { '/srv/teamspeak':
    ensure  => directory,
    mode    => '0700',
    owner   => 'teamspeak',
    group   => 'teamspeak',
    require => User['teamspeak'],
  }
  file { '/srv/teamspeak/ts3server.ini':
    ensure  => present,
    mode    => '0600',
    owner   => 'teamspeak',
    group   => 'teamspeak',
    source  => 'puppet:///modules/teamspeak/ts3server.ini',
    require => File['/srv/teamspeak'],
    notify  => Service['teamspeak'],
  }

  if $license {
    file { '/srv/teamspeak/licensekey.dat':
      ensure  => present,
      mode    => '0600',
      owner   => 'teamspeak',
      group   => 'teamspeak',
      source  => $license,
      replace => false,
      require => File['/srv/teamspeak'],
      notify  => Service['teamspeak'],
    }
  }

  file { '/etc/init.d/teamspeak':
    ensure => present,
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/teamspeak/teamspeak.init',
    notify => Exec['add-service-teamspeak'],
  }
  exec { 'add-service-teamspeak':
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    command     => 'chkconfig --add teamspeak',
    refreshonly => true,
    before      => Service['teamspeak'],
  }
  service { 'teamspeak':
    ensure => running,
    enable => true,
  }

}
