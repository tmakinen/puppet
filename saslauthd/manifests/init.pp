# Install saslauthd daemon.
#
# === Global variables
#
#   $saslauthd_mech:
#       Authentication mechanism to use. Defaults to system default.
#
class saslauthd::server {

    require sasl::saslauthd
    notice("Deprecation notice: saslauthd::server is deprecated, use sasl::saslauthd instead")

}


# Install saslauthd service config
#
# === Parameters
#
#   $name:
#       Service name.
#
# === Sample usage
#
# saslauthd::service { "Sendmail": }
#
define saslauthd::service() {

    sasl::saslauthd::service { $name: }
    notice("Deprecation notice: saslauthd::service is deprecated, use sasl::saslauthd::service instead")

}
