# Install and enable sysstat accounting.
#
class sysstat {

    package { "sysstat":
        ensure => installed,
    }

    case $::operatingsystem {
        "debian","ubuntu": {
            augeas { "enable-sysstat":
                context => "/files/etc/default/sysstat",
                changes => "set ENABLED true",
                require => Package["sysstat"],
                notify  => Service["sysstat"],
            }
        }
        "centos","redhat","fedora": {
        }
        default: {
            fail("sysstat not supported on ${::operatingsystem}")
        }
    }

    service { "sysstat":
        enable  => true,
        require => Package["sysstat"],
    }

}
