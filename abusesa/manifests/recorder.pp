# Install AbuseSA Recorder.
#
# === Parameters
#
# $enable:
#   Install and enable init script. Defaults to true.
#
# $datadir:
#   Recorder data directory. Defaults to /var/lib/recorder.
#
class abusesa::recorder(
  $enable=true,
  $datadir=undef,
) {

  if ! $abusesa_recorder_package {
    if $::abusesa_recorder_package_latest {
      $abusesa_recorder_package = $::abusesa_recorder_package_latest
    } else {
      fail('Must define $abusesa_recorder_package or $abusesa_recorder_package_latest')
    }
  }

  if $datadir != '/var/lib/recorder' {
    file { '/var/lib/recorder':
      ensure => link,
      target => $datadir,
    }
  }

  file { $datadir:
    ensure => directory,
    mode   => '0700',
    owner  => 'root',
    group  => 'root',
  }

  file { [
    '/etc/abusesa',
    '/etc/abusesa/probe.d',
    '/etc/abusesa/remote.d',
  ]:
    ensure => directory,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    before => Exec['/usr/local/src/abusesa-recorder-linux.sh'],
  }

  File['/etc/abusesa/probe.d', '/etc/abusesa/remote.d'] {
    purge   => true,
    force   => true,
    recurse => true,
    source  => 'puppet:///modules/custom/empty',
  }

  file { '/usr/local/src/abusesa-recorder-linux.sh':
    ensure => present,
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
    source => "puppet:///files/packages/${abusesa_recorder_package}",
  } ~>
  exec { 'rm -f /usr/local/recorder':
    refreshonly => true,
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    onlyif      => 'test -h /usr/local/recorder',
  } ->
  exec { '/usr/local/src/abusesa-recorder-linux.sh':
    creates => '/usr/local/recorder',
  }

  exec { 'abusesa-functions':
    refreshonly => true,
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    cwd         => '/usr/local/recorder',
    command     => 'sed s:@PREFIX@:/usr/local/recorder: abusesa-functions.in > /etc/abusesa/abusesa-functions',
    subscribe   => Exec['/usr/local/src/abusesa-recorder-linux.sh'],
  }

  if $enable == true {
    file { '/etc/init.d/abusesa-recorder':
      ensure  => present,
      mode    => '0755',
      owner   => 'root',
      group   => 'root',
      source  => '/usr/local/recorder/recorder-init.sh',
      require => Exec['/usr/local/src/abusesa-recorder-linux.sh'],
    } ~>
    exec { 'add-service-abusesa-recorder':
      refreshonly => true,
      path        => '/bin:/usr/bin:/sbin:/usr/sbin',
      command     => $::operatingsystem ? {
        'debian' => 'update-rc.d abusesa-recorder defaults',
        'ubuntu' => 'update-rc.d abusesa-recorder defaults',
        default  => 'chkconfig --add abusesa-recorder',
      },
    } ->
    service { 'abusesa-recorder':
      enable => true,
    }
  }

}


# Configure AbuseSA recorder.
#
# === Parameters
#
# $interface:
#   Capture interface. Defaults to $name.
#
# $snaplen:
#   Snaplen. Defaults to 65535.
#
# $keeptime:
#   Amount of data to keep. Defaults to 100GB.
#
# $blocksize:
#   Storage block size. Defaults to 1GB.
#
# $filter:
#   Optional filter expression.
#
# $remoteport:
#   Remote port. Defaults to 10000.
#
# $collab:
#   List of collabs for authentication.
#
# $probe:
#   Enable probe. Defaults to true.
#
# $remote:
#   Enable remote. Defaults to true.
#
# $services:
#   Manage probe and remote services. Defaults to true.
#
# === Sample usage
#
# abusesa::recorder::interface { 'eth0':
#   keeptime  => '500GB',
#   blocksize => '10GB',
#   filter    => 'host 192.168.1.1',
#   collab    => [ 'collabname:PageName' ],
# }
#
define abusesa::recorder::interface(
  $interface=undef,
  $snaplen='65535',
  $keeptime='100GB',
  $blocksize='1GB',
  $filter='',
  $remoteport='10000',
  $collab=[],
  $probeopt='',
  $remoteopt='',
  $probe=true,
  $remote=true,
  $services=true,
) {

  Class['abusesa::recorder'] -> Abusesa::Recorder::Interface[$name]

  if $interface {
    $interface_real = $interface
  } else {
    $interface_real = $name
  }

  file { "/var/lib/recorder/${name}":
    ensure => directory,
    mode   => '0700',
    owner  => 'root',
    group  => 'root',
  }

  if $probe == true and $services == true {
    $probe_notify = Service["probe-${name}"]
  } else {
    $probe_notify = undef
  }

  file { "/etc/abusesa/probe.d/${name}":
    ensure  => $probe ? {
      true  => present,
      false => absent,
    },
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    content => template('abusesa/recorder/probe.erb'),
    require => File["/var/lib/recorder/${name}"],
    notify  => $probe_notify,
  }

  if $services == true {
    service { "probe-${name}":
      ensure    => $probe ? {
        true  => running,
        false => stopped,
      },
      provider  => 'base',
      start     => "/etc/abusesa/probe.d/${name} start",
      restart   => "/etc/abusesa/probe.d/${name} restart",
      stop      => "pkill -f /var/run/probe/${name}.pid",
      status    => "pgrep -f /var/run/probe/${name}.pid",
      subscribe => Exec['/usr/local/src/abusesa-recorder-linux.sh'],
    }
  }

  if $remote == true and $services == true {
    $remote_notify = Service["remote-${name}"]
  } else {
    $remote_notify = undef
  }

  file { "/etc/abusesa/remote.d/${name}":
    ensure  => $remote ? {
      true  => present,
      false => absent,
    },
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    content => template('abusesa/recorder/remote.erb'),
    require => File["/var/lib/recorder/${name}"],
    notify  => $remote_notify,
  }

  if $services == true {
    service { "remote-${name}":
      ensure    => $remote ? {
        true  => running,
        false => stopped,
      },
      provider  => 'base',
      start     => "/etc/abusesa/remote.d/${name} start",
      restart   => "/etc/abusesa/remote.d/${name} restart",
      stop      => "pkill -f /var/run/remote/${name}.pid",
      status    => "pgrep -f /var/run/remote/${name}.pid",
      require   => Service["probe-${name}"],
      subscribe => Exec['/usr/local/src/abusesa-recorder-linux.sh'],
    }
  }

}
