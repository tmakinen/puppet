# Install abusesa-sensor.
#
class abusesa::sensor {

  if ! $abusesa_sensor_package {
    if $::abusesa_sensor_package_latest {
      $abusesa_sensor_package = $::abusesa_sensor_package_latest
    } else {
      fail('Must define $abusesa_sensor_package or $abusesa_sensor_package_latest')
    }
  }

  python::pip::install { 'abusesa-sensor.tar.gz':
    source => "puppet:///files/packages/${abusesa_sensor_package}",
  }

  Python::Pip::Install['abusesa.tar.gz'] ->
  Python::Pip::Install['abusesa-sensor.tar.gz']

}
