# Install AbuseSA Search.
#
# === Parameters
#
# $plugin:
#   Analysis plugin file.
#
# $solrurl:
#   URL to sorl.
#
# $logourl:
#   Path to logo image.
#
# $cssfile:
#   Path to css file.
#
# $webhosts:
#   List of search virtual hosts.
#
class abusesa::search(
  $plugin='solr-4.10.2-analysis-codenomicon.jar',
  $solrurl="https://${::homename}/solr/generic/",
  $logourl='img/Codenomicon_logo_small.png',
  $cssfile='css/code.css',
  $webhosts=undef,
) {

  if ! $abusesa_search_package {
    if $::abusesa_search_package_latest {
      $abusesa_search_package = $::abusesa_search_package_latest
    } else {
      fail('Must define $abusesa_search_package or $abusesa_search_package_latest')
    }
  }

  file { '/usr/local/src/abusesa-search.tar.gz':
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    source => "puppet:///files/packages/${abusesa_search_package}",
  }

  util::extract::tar { '/usr/local/src/abusesa-search':
    ensure  => latest,
    strip   => '1',
    source  => '/usr/local/src/abusesa-search.tar.gz',
    require => File['/usr/local/src/abusesa-search.tar.gz'],
    before  => Python::Setup::Install['/usr/local/src/abusesa-search'],
  }

  python::setup::install { '/usr/local/src/abusesa-search': }

  package { 'python-BeautifulSoup':
    ensure => installed,
    name   => $::operatingsystem ? {
      'debian' => 'python-beautifulsoup',
      'ubuntu' => 'python-beautifulsoup',
      default  => 'python-BeautifulSoup',
    }
  }

  file { '/srv/solr/cores/generic':
    ensure  => directory,
    mode    => '0660',
    owner   => 'solr',
    group   => 'solr',
    source  => '/usr/local/src/abusesa-search/config/solr/cores/generic',
    recurse => true,
    purge   => true,
    force   => true,
    require => Util::Extract::Tar['/usr/local/src/abusesa-search'],
    notify  => Service['solr'],
  }

  file { '/srv/solr/cores/generic/conf/_rest_managed.json':
    ensure => present,
    mode   => '0660',
    owner  => 'solr',
    group  => 'solr',
    before => Service['solr'],
  }

  file { '/srv/solr/cores/lib/solr-analysis-codenomicon.jar':
    ensure  => present,
    mode    => '0660',
    owner   => 'solr',
    group   => 'solr',
    source  => "/usr/local/src/abusesa-search/bin/${plugin}",
    require => Util::Extract::Tar['/usr/local/src/abusesa-search'],
    notify  => Service['solr'],
  }
  file { '/srv/solr/cores/lib/commons-net-3.1.jar':
    ensure  => present,
    mode    => '0660',
    owner   => 'solr',
    group   => 'solr',
    source  => '/usr/local/src/abusesa-search/search/analysis-codenomicon/lib/commons-net-3.1.jar',
    require => Util::Extract::Tar['/usr/local/src/abusesa-search'],
    notify  => Service['solr'],
  }
  file { '/srv/solr/cores/lib/java-ipv6-0.8.jar':
    ensure  => present,
    mode    => '0660',
    owner   => 'solr',
    group   => 'solr',
    source  => '/usr/local/src/abusesa-search/search/analysis-codenomicon/lib/java-ipv6-0.8.jar',
    require => Util::Extract::Tar['/usr/local/src/abusesa-search'],
    notify  => Service['solr'],
  }

  $htdocs = $::operatingsystem ? {
    'ubuntu' => '/usr/local/share/abusesa-search/htdocs',
    default  => '/usr/share/abusesa-search/htdocs',
  }

  file { "${htdocs}/js/Config.js":
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('abusesa/search/Config.js.erb'),
    require => Python::Setup::Install['/usr/local/src/abusesa-search'],
  }

  if $webhosts {
    abusesa::search::configwebhost { $webhosts:
      htdocs => $htdocs,
    }
  }

}


# Enable AbuseSA Search for virtual host.
#
define abusesa::search::configwebhost($htdocs) {

  if ! defined(Abusesa::Configwebhost[$name]) {
    abusesa::configwebhost { $name: }
  }

  file { "/srv/www/https/${name}/abusesa/search":
    ensure => link,
    target => $htdocs,
  }

}
