# Install AbuseSA.
#
# === Parameters
#
# $datadir:
#   AbuseSA home directory. Defaults to /var/lib/abusesa.
#
# $botnets:
#   Array of botnet paths to start at boot.
#
# $botuser:
#   User to run bots as. Defaults to abusesa.
#
class abusesa(
  $datadir='/var/lib/abusesa',
  $botnets=undef,
  $botuser='abusesa',
) {

  if ! $abusesa_package {
    if $::abusesa_package_latest {
      $abusesa_package = $::abusesa_package_latest
    } else {
      fail('Must define $abusesa_package or $abusesa_package_latest')
    }
  }

  include user::system
  realize(User['abusesa'], Group['abusesa'])

  if $datadir != '/var/lib/abusesa' {
    file { '/var/lib/abusesa':
      ensure => link,
      target => $datadir,
    }
  }

  file { $datadir:
    ensure => directory,
    mode   => '2770',
    owner  => 'abusesa',
    group  => 'abusesa',
  }

  file { '/var/lib/abusesa/.profile':
    ensure  => present,
    replace => false,
    mode    => '0600',
    owner   => 'abusesa',
    group   => 'abusesa',
    content => "umask 007\n",
  }

  python::pip::install { 'abusesa.tar.gz':
    source => "puppet:///files/packages/${abusesa_package}",
  }

  Python::Pip::Install['abusehelper.tar.gz'] ->
  Python::Pip::Install['abusesa.tar.gz']

  Python::Setup::Install['/usr/local/src/opencollab'] ->
  Python::Pip::Install['abusesa.tar.gz']

  if $botnets {
    class { 'abusehelper::init':
      botnets => $botnets,
      botuser => $botuser,
    }
  }

}


# Create AbuseSA htdocs root.
#
define abusesa::configwebhost() {

  file { "/srv/www/https/${name}/abusesa":
    ensure => directory,
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }
  file { "/srv/www/https/${name}/abusesa/index.html":
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => '',
  }

}
