
# Install name service cache daemon
#
class nscd {

    package { "nscd":
        ensure => installed,
    }

    service { "nscd":
        ensure  => running,
        enable  => true,
        require => [ File["/etc/netgroup"], Package["nscd"], ],
    }

    if !defined(File["/etc/netgroup"]) {
        file { "/etc/netgroup":
            ensure => present,
            mode   => "0644",
            owner  => "root",
            group  => "root",
        }
    }

}
