# Install mongodb server.
#
class mongodb($datadir='/srv/mongodb') {

  package { [
    'mongodb',
    'mongodb-server',
  ]:
    ensure => installed,
  }

  if $datadir != '/srv/mongodb' {
    file { '/srv/mongodb':
      ensure  => link,
      target  => $datadir,
    }
  }

  file { $datadir:
    ensure  => directory,
    mode    => '0770',
    owner   => 'mongodb',
    group   => 'mongodb',
    require => Package['mongodb-server'],
  }

  if versioncmp($::operatingsystemrelease, "7") >= 0 {
    $config = '/etc/mongod.conf'
  } else {
    $config = '/etc/mongodb.conf'
  }

  file { $config:
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/mongodb/mongod.conf',
    notify => Service['mongod'],
  }

  service { 'mongod':
    ensure  => running,
    enable  => true,
    require => File['/srv/mongodb'],
  }

}
