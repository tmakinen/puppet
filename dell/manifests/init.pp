# Prerequisites for Dell tools
#
class dell::common {

    case $::operatingsystem {
        "centos","redhat": {
            include yum::repo::dell
        }
        default: {
            fail("Dell modules not supported in ${::operatingsystem}")
        }
    }

}


# Tools and services for Dell iDRAC7 management
#
class dell::idrac7 {
    include dell::common

    package { 'srvadmin-idrac7':
        ensure  => installed,
        require => Class["yum::repo::dell"],
    }

    # Enable OpenManage System services
    exec { "srvadmin-service-enable":
        path    => "/bin:/sbin",
        command => "/opt/dell/srvadmin/sbin/srvadmin-services.sh enable",
        unless  => "chkconfig --list dataeng | grep 3:on >/dev/null",
        user    => "root",
        group   => "root",
        require => Exec["srvadmin-service-start"],
    }

    # Start OpenManage System services
    exec { "srvadmin-service-start":
        command => "/opt/dell/srvadmin/sbin/srvadmin-services.sh start",
        unless  => "/usr/bin/pgrep -f /opt/dell/srvadmin/sbin/dsm_sa_datamgrd",
        user    => "root",
        group   => "root",
        require => Package["srvadmin-idrac7"],
    }

}
