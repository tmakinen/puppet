
# Install Vim (Vi IMproved)
#
class vim {

    package { "vim":
        ensure => installed,
        name   => $::operatingsystem ? {
            "centos" => "vim-enhanced",
            "redhat" => "vim-enhanced",
            "fedora" => "vim-enhanced",
            default  => "vim",
        },
    }

    if $::puppetversion =~ /^2\./ {
        Package["vim"] {
            flavor => "gtk2",
        }
    }

}
