# Install Lynx WWW browser
#
# === Global variables:
#
#   $www_default_url:
#       Default starting URL.
#
class lynx {

    if ! $::www_default_url {
        $www_default_url = "http://www.${::domain}"
    }

    case $::operatingsystem {
        "openbsd": {
            exec { "add-local-lynx-config":
                command => "echo 'INCLUDE:/etc/lynx-site.cfg' >> /etc/lynx.cfg",
                path    => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
                user    => "root",
                unless  => "fgrep 'INCLUDE:/etc/lynx-site.cfg' /etc/lynx.cfg",
            }
            $config = "/etc/lynx-site.cfg"
            $package = undef
        }
        "ubuntu": {
            if versioncmp($::operatingsystemrelease, "16.04") < 0 {
                $config = "/etc/lynx-cur/local.cfg"
                $package = "lynx-cur"
            } else {
                $config = "/etc/lynx/local.cfg"
                $package = "lynx"
            }
        }
        default: {
            $config = "/etc/lynx-site.cfg"
            $package = "lynx"
        }
    }

    if $package {
        package { "lynx":
            ensure => installed,
            name   => $package,
            before => [
                File["/usr/local/bin/html2text"],
                File["lynx-site.cfg"],
            ],
        }
    }

    file { "lynx-site.cfg":
        ensure  => present,
        name    => $config,
        content => template("lynx/lynx-site.cfg.erb"),
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
    }

    file { "/usr/local/bin/html2text":
        ensure => present,
        source => "puppet:///modules/lynx/html2text",
        mode   => "0755",
        owner  => "root",
        group  => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
    }

}
