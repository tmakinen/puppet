# Install cairo.
#
class cairo {

    package { "cairo":
        ensure => installed,
        name   => $::operatingsystem ? {
            debian  => "libcairo2",
            ubuntu  => "libcairo2",
            default => "cairo",
        },
    }

}


# Install python bindings for cairo.
#
class cairo::python inherits cairo {

    package { "pycairo":
        ensure => installed,
        name   => $::operatingsystem ? {
            debian  => "python-cairo",
            ubuntu  => "python-cairo",
            default => "pycairo",
        },
    }

}
