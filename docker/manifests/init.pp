# Install Docker.
#
class docker {

  if ! ($::operatingsystem in ["CentOS","RedHat"] and versioncmp($::operatingsystemrelease, "7") > 0) {
    fail('Docker is supported only on EL 7')
  }

  package { 'docker':
    ensure => installed,
  } ->
  service { 'docker':
    ensure  => running,
    enable  => true,
  }

  python::pip::install { 'docker-compose':
    ensure => '1.8.1',
  }

}


# Load docker image.
#
define docker::image($source) {

  require docker
  require gnu::tar

  file { "/usr/local/src/${name}":
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => 'wheel',
    source => $source,
  }

  exec { "docker-load-${name}":
    refreshonly => true,
    path        => '/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin',
    command     => "/bin/sh -c 'umask 022; docker load --input /usr/local/src/${name}'",
    subscribe   => File["/usr/local/src/${name}"],
  }

}
