
# Install support for setting D-Conf default settings
#
class gnome::dconf {

    file { "/etc/dconf/db/site.d":
        ensure  => directory,
        source  => "puppet:///modules/custom/empty",
        mode    => "0755",
        owner   => "root",
        group   => "root",
        purge   => true,
        force   => true,
        recurse => true,
        notify  => Exec["dconf-update"],
    }

    file { "/etc/dconf/profile/user":
        ensure  => present,
        content => "user-db:user\nsystem-db:site\n",
        mode    => "0644",
        owner   => "root",
        group   => "root",
        notify  => Exec["dconf-update"],
    }

    exec { "dconf-update":
        command     => "dconf update",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        user        => "root",
        refreshonly => true,
    }

}


# Install D-Conf defaults setting file
#
# === Parameters:
#
#     $name:
#         Settings name
#
#     $source:
#         File to use as source for settings
#
# === Sample usage:
#
# gnome::dconf::default { "desktop":
#     source => "puppet:///files/common/gnome/desktop.conf",
# }
#
define gnome::dconf::default($source) {

    include gnome::dconf

    file { "/etc/dconf/db/site.d/${name}":
        ensure  => present,
        source  => $source,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => File["/etc/dconf/db/site.d"],
        notify  => Exec["dconf-update"],
    }

}


# Install D-Conf editor
#
class gnome::dconf::editor {

    package { "dconf-editor":
        ensure => installed,
    }

}


# Set GNOME gconf values.
#
# === Parameters
#
#   $name:
#       Key to update
#   $value:
#       Value for given key
#   $source:
#       Source to update. Valid values are "default" and "mandatory",
#       defaults to "default".
#   $type:
#       Value type. Valid values are "string" and "int", defaults to
#       string.
#
define gnome::gconf($value, $source = "default", $type = "string", $ltype = "") {

    case $source {
        "mandatory": {
            $xml = "/etc/gconf/gconf.xml.mandatory"
        }
        "default": {
            $xml = "/etc/gconf/gconf.xml.defaults"
        }
        default: {
            fail("Invalid gnome::gconf source.")
        }
    }

    case $ltype {
        "string": {
            $param = "--list-type 'string'"
        }

        default: {
            $param = ""
        }
    }

    exec { "gconftool-2 --direct --config-source xml:readwrite:${xml} --type ${type} ${param} --set '${name}' '${value}'":
        path   => "/bin:/usr/bin:/sbin:/usr/sbin",
        unless => "test \"`gconftool-2 --direct --config-source xml:readwrite:${xml} --get '${name}'`\" = '${value}'",
    }

}


# Install GConf editor
#
class gnome::gconf::editor {

    package { "gconf-editor":
        ensure => installed,
    }

}
