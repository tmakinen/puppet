module Puppet::Parser::Functions
    newfunction(:is_array, :type => :rvalue) do |args|
        if args.length != 1
            raise Puppet::ParseError, ("is_array(): wrong number of arguments (#{args.length}; must be 2)")
        end
        args[0].is_a?(Array)
    end
end
