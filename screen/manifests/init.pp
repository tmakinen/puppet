# Install screen.
#
class screen {

    package { "screen":
        ensure => installed,
    }

    exec { "set-screen-defnonblock":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        user    => "root",
        command => "echo 'defnonblock on' >> /etc/screenrc",
        unless  => "fgrep -x 'defnonblock on' /etc/screenrc",
        require => Package["screen"],
    }

}
