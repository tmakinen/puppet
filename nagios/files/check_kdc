#!/bin/sh
#
# Check kerberos 5 KDC server.
#
# usage:
#
#     check_kdc -H <hostname> [-P <principal>] [-k <keytab>]
#

print_usage() {
    echo "`basename $0` -H <hostname> -r <realm> [-P <principal>] [-k <keytab>]"
}

# set defaults
TARGET=""
REALM=""
PRINCIPAL="host/`hostname`"
KEYTAB="/etc/krb5.keytab"

while test -n "$1" ; do
    case "$1" in
	--help|-h)
	    print_usage
	    exit 0
	    ;;
	-H)
	    TARGET="$2"
	    shift
	    ;;
	-P)
	    PRINCIPAL="$2"
	    shift
	    ;;
	-k)
	    KEYTAB="$2"
	    shift
	    ;;
	-r)
	    REALM="$2"
	    shift
	    ;;
	*)
	    echo "Unknown argument: $1" 1>&2
	    print_usage 1>&2
	    exit 3
    esac
    shift
done

if [ "${TARGET}" = "" ]; then
    echo "Missing hostname" 1>&2
    print_usage 1>&2
    exit 3
elif [ "${REALM}" = "" ]; then
    # try to get realm from principal
    REALM=`echo "${PRINCIPAL}" | sed -n 's/.*@\(.*\)$/\1/p'`
    if [ "${REALM}" = "" ]; then
	echo "Missing realm" 1>&2
	print_usage 1>&2
	exit 3
    fi
fi

export KRB5_CONFIG="`mktemp /tmp/krb5.conf.XXXXXXXXXX`"

cat <<EOF > ${KRB5_CONFIG}
[libdefaults]
  default_realm = ${REALM}
  dns_lookup_realm = false
  dns_lookup_kdc = false

[realms]
  ${REALM} = {
    kdc = ${TARGET}
  }
EOF

MESSAGE="`kinit -k -t ${KEYTAB} -c MEMORY: -P ${PRINCIPAL} 2>&1`"
if [ $? -eq 0 ]; then
    MESSAGE="OK"
    RETVAL=0
else
    MESSAGE="CRITICAL: `echo ${MESSAGE} | sed -e 's/^kinit: //'`"
    RETVAL=2
fi

kdestroy -c MEMORY: > /dev/null 2>&1

rm -f ${KRB5_CONFIG}

echo ${MESSAGE}
exit ${RETVAL}
